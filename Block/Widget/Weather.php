<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Weather extends Template implements BlockInterface
{
    /**
     * @var string
     */
    protected $_template = 'widget/weather.phtml';

    /**
     * @return string
     */
    public function getWeatherLayout(): string
    {
        return $this->getData('weather_layout');
    }

    /**
     * @return string
     */
    public function getWeatherTitle(): ?string
    {
        return $this->getData('weather_title');
    }
}
