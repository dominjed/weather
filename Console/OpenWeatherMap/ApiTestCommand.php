<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Console\OpenWeatherMap;

use DominJed\Weather\Model\WeatherInformationFactory as WeatherInformationModelFactory;
use DominJed\Weather\Model\WeatherInformation as WeatherInformationModel;
use DominJed\Weather\Model\ResourceModel\WeatherInformation as WeatherInformationResourceModel;
use DominJed\Weather\Service\OpenWeatherMap\CallService;
use DominJed\Weather\Service\ServiceInitializeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ApiTestCommand extends Command
{
    const COMMAND_NAME = 'dmj:openweathermap:call';

    const COMMAND_DESC = 'Get current data from OpenWeatherMap serwer by API.';

    /**
     * @var SymfonyStyle
     */
    protected $_io;

    /**
     * @var CallService
     */
    protected $callService;

    /**
     * @var WeatherInformationModelFactory
     */
    protected $weatherInformationModelFactory;

    /**
     * @var WeatherInformationResourceModel $weatherInformationResourceModel
     */
    protected $weatherInformationResourceModel;

    /**
     * @param string $name
     * @param CallService $callService
     * @param WeatherInformationModelFactory $weatherInformationModelFactory
     * @param WeatherInformationResourceModel $weatherInformationResourceModel
     */
    public function __construct(
        string $name = null,
        CallService $callService,
        WeatherInformationModelFactory $weatherInformationModelFactory,
        WeatherInformationResourceModel $weatherInformationResourceModel
    )
    {
        parent::__construct($name);
        $this->callService = $callService;
        $this->weatherInformationModelFactory = $weatherInformationModelFactory;
        $this->weatherInformationResourceModel = $weatherInformationResourceModel;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription(self::COMMAND_DESC)
            ->addOption(
                'save',
                's',
                InputOption::VALUE_OPTIONAL,
                'Save to database current data (Y or n), n is default',
                'n'
            );

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $this->_io = new SymfonyStyle($input, $output);

        $this->_io->title('Get current data from OpenWeatherMap');
        $this->_io->writeln('init');

        if (!$this->callService instanceof ServiceInitializeInterface) {
            $this->_io->error('Incorrect type of object! Expected of ' . ServiceInitializeInterface::class);
            die;
        }

        $this
            ->callService
            ->init();
        $this->_io->writeln('call');
        $responseData = $this->callService->call();

        if (!filter_var($responseData['status'], FILTER_VALIDATE_BOOLEAN)) {
            $this->_io->error('Something went wrong!');
            print_r($responseData);
            die;
        }

        print_r($responseData);

        if ('Y' === $input->getOption('save')) {
            /** @var WeatherInformationModel $weatherInformationModel */
            $weatherInformationModel = $this
                ->weatherInformationModelFactory
                ->create();
            $weatherInformationModel->setData($responseData['weather']);
            $this
                ->weatherInformationResourceModel
                ->save($weatherInformationModel);

            $this->_io->writeln('Current data saved');
        }
        echo "\n";
        $this->_io->success('Command finished!');
    }
}
