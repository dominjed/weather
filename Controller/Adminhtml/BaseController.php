<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class BaseController extends Action
{
    /**
     * {@inheritDoc}
     */
    protected function _isAllowed(): bool
    {
        return $this->_authorization->isAllowed('DominJed_Weather::informations');
    }
}
