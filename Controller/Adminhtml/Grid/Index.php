<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Controller\Adminhtml\Grid;

use DominJed\Weather\Controller\Adminhtml\BaseController;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page as ResultPage;
use Magento\Framework\View\Result\Page as FrameworkPage;

class Index extends BaseController
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var ResultPage
     */
    protected $_resultPage;

    /**
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritDoc}
     */
    public function execute(): ResultPage
    {
        $this->_setPageData();

        return $this->getResultPage();
    }

    /**
     * @return ResultPage
     */
    public function getResultPage(): ResultPage
    {
        if (!$this->_resultPage) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }

        return $this->_resultPage;
    }

    /**
     * @return Index
     */
    protected function _setPageData(): Index
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magento_Backend::content');
        $resultPage
            ->getConfig()
            ->getTitle()
            ->prepend((__('Weather informations')));

        return $this;
    }
}
