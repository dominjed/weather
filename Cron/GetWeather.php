<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Cron;

use DominJed\Weather\Exception\WeatherException;
use DominJed\Weather\Service\ServiceInitializeInterface;
use Magento\Framework\App\ObjectManager;
use Psr\Log\LoggerInterface;
use DominJed\Weather\Helper\Data;
use DominJed\Weather\Model\WeatherInformationFactory as WeatherInformationModelFactory;
use DominJed\Weather\Model\WeatherInformation as WeatherInformationModel;
use DominJed\Weather\Model\ResourceModel\WeatherInformation as WeatherInformationResourceModel;

class GetWeather
{
    /**
     * @var LoggerInterface
     */
    protected $_loggerInterface;

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * @var WeatherInformationModelFactory
     */
    protected $weatherInformationModelFactory;

    /**
     * @var WeatherInformationResourceModel
     */
    protected $weatherInformationResourceModel;

    /**
     * @param LoggerInterface $loggerInterface
     * @param Data $dataHelper
     * @param WeatherInformationModelFactory $weatherInformationModelFactory
     * @param WeatherInformationResourceModel $weatherInformationResourceModel
     */
    public function __construct(
        LoggerInterface $loggerInterface,
        Data $dataHelper,
        WeatherInformationModelFactory $weatherInformationModelFactory,
        WeatherInformationResourceModel $weatherInformationResourceModel
    )
    {
        $this->_loggerInterface = $loggerInterface;
        $this->dataHelper = $dataHelper;
        $this->weatherInformationModelFactory = $weatherInformationModelFactory;
        $this->weatherInformationResourceModel = $weatherInformationResourceModel;
    }

    /**
     * @return void
     */
    public function execute()
    {
        if ($this->dataHelper->isEnabled()) {

            try {
                $callServiceName = 'DominJed\\Weather\\Service\\' . $this->dataHelper->getServiceType() . '\\CallService';
                if (!class_exists($callServiceName)) {
                    throw new WeatherException('CallService [' . $callServiceName . '] does not existed');
                }
                $callService = ObjectManager::getInstance()->create($callServiceName);

                $this->saveWeatherData($callService);

            } catch (WeatherException $e) {
                $this->_loggerInterface->warning('WeatherException: ' . $e->getMessage());
            } catch (\Exception $e) {
                $this->_loggerInterface->warning('General Exception: ' . $e->getMessage());
            }
        }
    }

    /**
     * @param ServiceInitializeInterface $callService
     * @return GetWeather
     * @throws WeatherException
     */
    private function saveWeatherData(ServiceInitializeInterface $callService): GetWeather
    {
        $responseData = $callService
            ->init()
            ->call();

        if (!filter_var($responseData['status'], FILTER_VALIDATE_BOOLEAN)) {
            throw new WeatherException($responseData['error']['code'] . ', ' . $responseData['error']['msg']);
        }

        /** @var WeatherInformationModel $weatherInformationModel */
        $weatherInformationModel = $this
            ->weatherInformationModelFactory
            ->create();
        $weatherInformationModel->setData($responseData['weather']);
        $this
            ->weatherInformationResourceModel
            ->save($weatherInformationModel);

        return $this;
    }
}
