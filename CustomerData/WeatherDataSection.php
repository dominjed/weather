<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use DominJed\Weather\Model\ResourceModel\WeatherInformation\CollectionFactory;
use DominJed\Weather\Model\ResourceModel\WeatherInformation\Collection;
use Magento\Framework\Data\Collection\AbstractDb;

class WeatherDataSection implements SectionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param CollectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $weatherData = $this->getWeatherData();
        return [
            'status' => (!empty($weatherData)),
            'data' => $weatherData
        ];
    }

    /**
     * @return array
     */
    private function getWeatherData(): array
    {
        /** @var Collection $collection */
        $collection = $this
            ->_collectionFactory
            ->create();

        $weatherData = $collection
            ->setOrder('created_at', AbstractDb::SORT_ORDER_DESC)
            ->getFirstItem();

        if ($weatherData->isEmpty()) {
            return [];
        }

        return $weatherData->toArray();
    }
}
