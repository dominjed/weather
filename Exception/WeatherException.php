<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Exception;

class WeatherException extends \Exception
{
}
