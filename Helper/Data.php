<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const BASE_PATH = 'dmj_weather_configuration/weather_api_config/';

    const IS_ENABLED = 'weather_enable';

    const SERVICE_TYPE = 'weather_service_type';

    const API_KEY = 'weather_api_key';

    const UNITS = 'weather_units';

    const LANG = 'weather_lang';

    const CITY = 'weather_city';

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return filter_var($this->scopeConfig->getValue(
            self::BASE_PATH . self::IS_ENABLED,
            ScopeInterface::SCOPE_STORE
        ), FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return (string)$this->scopeConfig->getValue(
            self::BASE_PATH . self::API_KEY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getServiceType(): string
    {
        return $this->scopeConfig->getValue(
            self::BASE_PATH . self::SERVICE_TYPE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getUnits(): string
    {
        return $this->scopeConfig->getValue(
            self::BASE_PATH . self::UNITS,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->scopeConfig->getValue(
            self::BASE_PATH . self::LANG,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->scopeConfig->getValue(
            self::BASE_PATH . self::CITY,
            ScopeInterface::SCOPE_STORE
        );
    }
}
