<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Helper\Source;

use Magento\Ui\Component\Listing\Columns\Column;

class CoordData extends Column
{
    const FIELD_NAME = 'coord';

    /**
     * {@inheritdoc}
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if (self::FIELD_NAME === $fieldName) {
                    if (!empty($item[$fieldName])) {
                        $array = json_decode($item[$fieldName], true);
                        $string = '';
                        foreach ($array as $k => $v) {
                            $string .= $k . ': ' . $v . ', ';
                        }
                        $item[$fieldName] = rtrim($string, ', ');
                    } else {
                        $item[$fieldName] = '-';
                    }
                    continue;
                }
            }
        }

        return $dataSource;
    }
}
