<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Helper\Source;

use DominJed\Weather\Service\OpenWeatherMap\CallService;
use Magento\Framework\Option\ArrayInterface;

class ServiceTypeSelect implements ArrayInterface
{
    /**
     * {@inheritDoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => CallService::SERVICE_TYPE,
                'label' => CallService::SERVICE_TYPE
            ]
        ];
    }
}
