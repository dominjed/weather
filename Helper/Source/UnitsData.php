<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Helper\Source;

use Magento\Ui\Component\Listing\Columns\Column;

class UnitsData extends Column
{
    /**
     * @var array
     */
    protected $fieldsUnits = [
        'cloud_value',
        'humidity_value',
        'precipitation_value',
        'pressure_value',
        'temperature_min_value',
        'temperature_max_value',
        'temperature_now_value',
        'wind_speed_value',
        'wind_direction_value'
    ];

    /**
     * {@inheritdoc}
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as $k => &$item) {
                if (in_array($fieldName, $this->fieldsUnits)) {
                    if (isset($item[$fieldName])) {
                        $unitFieldName = substr($fieldName, 0, -5) . 'unit';
                        if (isset($dataSource['data']['items'][$k][$unitFieldName])) {
                            $item[$fieldName] = $item[$fieldName] . ' ' . $dataSource['data']['items'][$k][$unitFieldName];
                        }
                    } else {
                        $item[$fieldName] = '-';
                    }
                    continue;
                }
            }
        }

        return $dataSource;
    }
}
