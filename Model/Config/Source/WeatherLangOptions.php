<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\Config\Source;

class WeatherLangOptions
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'pl', 'label' => 'PL'],
            ['value' => 'en', 'label' => 'EN'],
            ['value' => 'de', 'label' => 'DE']
        ];
    }
}
