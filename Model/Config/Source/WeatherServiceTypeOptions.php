<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\Config\Source;

use DominJed\Weather\Service\OpenWeatherMap\CallService;

class WeatherServiceTypeOptions
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => CallService::SERVICE_TYPE, 'label' => 'OpenWeatherMap']
        ];
    }
}
