<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\Config\Source;

class WeatherUnitsOptions
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'metric', 'label' => __('Metric')],
            ['value' => 'imperial', 'label' => __('Imperial')]
        ];
    }
}
