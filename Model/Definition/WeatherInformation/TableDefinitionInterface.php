<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\Definition\WeatherInformation;

interface TableDefinitionInterface
{
    const TABLE_NAME = 'dmj_weather_information';
}
