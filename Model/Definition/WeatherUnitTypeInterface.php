<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\Definition;

interface WeatherUnitTypeInterface
{
    const PERCENT_UNIT = '%';

    const RAIN_UNIT = 'mm';

    const PRESSURE_UNIT = 'hPa';
}
