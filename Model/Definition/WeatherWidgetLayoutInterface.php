<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\Definition;

interface WeatherWidgetLayoutInterface
{
    const BASIC = 'basic';

    const EXTENDED = 'extended';

    const ADVANCED = 'advanced';
}
