<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\ResourceModel;

use DominJed\Weather\Model\Definition\WeatherInformation\TableDefinitionInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class WeatherInformation extends AbstractDb
{
    /**
     * {@inheritDoc}
     */
    protected $_serializableFields = [
        'coord' => [null, []]
    ];

    /**
     * {@inheritDoc}
     */
    protected function _construct()
    {
        $this->_init(TableDefinitionInterface::TABLE_NAME, 'id');
    }
}
