<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model\ResourceModel\WeatherInformation;

use Magento\Framework\DataObject;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use DominJed\Weather\Model\WeatherInformation;
use DominJed\Weather\Model\ResourceModel\WeatherInformation as WeatherInformationResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            WeatherInformation::class,
            WeatherInformationResourceModel::class
        );
    }
}
