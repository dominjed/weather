<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Model;

use DominJed\Weather\Model\Definition\WeatherInformation\TableDefinitionInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use DominJed\Weather\Model\ResourceModel\WeatherInformation as WeatherInformationResourceModel;

class WeatherInformation extends AbstractModel implements IdentityInterface
{
    /**
     * {@inheritDoc}
     */
    protected $_cacheTag = TableDefinitionInterface::TABLE_NAME;

    /**
     * {@inheritDoc}
     */
    protected $_eventPrefix = TableDefinitionInterface::TABLE_NAME;

    /**
     * {@inheritDoc}
     */
    protected function _construct()
    {
        $this->_init(WeatherInformationResourceModel::class);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentities()
    {
        return [TableDefinitionInterface::TABLE_NAME . '_' . $this->getId()];
    }
}
