## Info

The module retrieves information about the current weather and displays it. It uses OpenWeatherMap API from https://openweathermap.org

#### Tested on the version

2.3.*

## Installation and configuration

#### Install the module

**IMPORTANT!**

The module requires installation of additional libraries, therefore it is recomended to install using **composer**. Installation process below:

add the repository (https for public)

~~~~
    composer config repositories.dominjed_weather vcs https://djedrzejczyk@bitbucket.org/dominjed/weather.git
~~~~

or if you have access via ssh

~~~~
    composer config repositories.dominjed_weather vcs git@bitbucket.org:dominjed/weather.git
~~~~

download and install module

~~~~
    composer require dominjed/weather:"1.*"
~~~~

run Upgrade Setup

~~~~
    php bin/magento setup:upgrade
~~~~

run compilation

~~~~
    php bin/magento setup:di:compile
~~~~

generate static files for your languages

~~~~
    php bin/magento setup:static-content:deploy en_US
~~~~

cache cache

~~~~
    php bin/magento clear:cache
~~~~

#### Project configuration

Your project should have a properly configured cron according to Magento2 documentation
[Magento2.3 CRON configuration](https://devdocs.magento.com/guides/v2.3/config-guide/cli/config-cli-subcommands-cron.html)

#### Configuration the module

Log in to the admin panel and follow the steps:

- Go to `Stores->Settings->Configuration->DominJed->Weather`
- Enable configuration and put API key to applied weather map (now it is only OpenWeatherMap)
- Configure the rest of the fields like city, lang, metrics

#### Downloading the first weather data (optional, but recomended)

Before CRON downloads the first weather data, you can do the test yourself and save the data to the database.
You will then be able to see the weather data in the project before CRON updates the background data.

run command (with option -s Y) and wait to finish proccess

~~~~
    php bin/magento dmj:openweathermap:call -s Y
~~~~

#### Grid with history informations about weather

Go to `Content->DominJed Weather->Weather` informations. There is grid with informations about history of weather data.

#### Show weather data on the front of project

Configure the widget:

- Go to `Content->Elements->Widgets`
- Add new widget
- Select type of: **DominJed Weather Widget** and **Design Theme** (module was tested on Magento Luma -recomended), click "continue"
- Write **Widget title** and select **Store views** (e.g. "All stores views")
- Click **Add layout update** button and "Display On" **All pages**
- Select Container. Usually module was tested on **Page footer**

- Go to **Widget options** tab and write **Weather title** (optional) and select **Weather layout** (extended -recomended)
- **Save** your widget
- **Clear cache**! Remember -after changing in widget configuration always you have to clear cache.

## Further development of the module

The module is prepared so that you can add more weather services (currently it is only OpenWeatherMap)

#### New weather service

Follow the steps to program the new weather service:

- Create `DominJed\Weather\Service\[name of service]\CallService` class which implements `DominJed\Weather\Service\ServiceInitializeInterface`
- Do it exactly like the OpenWeatherMap website, call method has to return array with informations about weather to database
- Add option with name your service to `DominJed\Weather\Model\Config\Source\WeatherServiceTypeOptions`
- Add option with name your service to `DominJed\Weather\Helper\Source\ServiceTypeSelect`
- Optionally, you can add Command for API tests similar to OpenWeatherMap. Look: `DominJed\Weather\Command\OpenWeatherMap\ApiTestCommand`

run command

~~~~
    php bin/magento setup:upgrade && php bin/magento setup:di:compile && php bin/magento clear:cache
~~~~

now you can select your new service in module configuration

#### License

MIT

**Enjoy!** ;)