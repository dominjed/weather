<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Service\OpenWeatherMap;

use Cmfcmf\OpenWeatherMap;
use Cmfcmf\OpenWeatherMap\Exception as OWMException;
use DominJed\Weather\Helper\Data;
use DominJed\Weather\Model\Definition\WeatherUnitTypeInterface;
use DominJed\Weather\Service\ServiceInitializeInterface;
use Http\Factory\Guzzle\RequestFactory;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Psr\Log\LoggerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class CallService implements ServiceInitializeInterface
{
    const SERVICE_TYPE = 'OpenWeatherMap';

    /**
     * @var Data
     */
    protected $_dataHelper;

    /**
     * @var OpenWeatherMap
     */
    protected $_owm;

    /**
     * @var LoggerInterface
     */
    protected $_loggerInterface;

    /**
     * @var TimezoneInterface
     */
    protected $_timezone;

    /**
     * @param Data $dataHelper
     * @param LoggerInterface $loggerInterface
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        Data $dataHelper,
        LoggerInterface $loggerInterface,
        TimezoneInterface $timezone
    )
    {
        $this->_dataHelper = $dataHelper;
        $this->_loggerInterface = $loggerInterface;
        $this->_timezone = $timezone;
    }

    /**
     * @return ServiceInitializeInterface
     */
    public function init(): ServiceInitializeInterface
    {
        $httpRequestFactory = new RequestFactory();
        $httpClient = GuzzleAdapter::createWithConfig([]);

        $this->_owm = new OpenWeatherMap(
            $this->_dataHelper->getApiKey(),
            $httpClient,
            $httpRequestFactory
        );

        return $this;
    }

    /**
     * @return array
     */
    public function call(): array
    {
        try {
            $weather = $this->_owm->getWeather(
                $this->_dataHelper->getCity(),
                $this->_dataHelper->getUnits(),
                $this->_dataHelper->getLang()
            );

            return $this->_prepareData($weather);
        } catch (OWMException $e) {
            return $this->_prepareError(
                'OpenWeatherMap exception: ' . $e->getMessage(),
                $e->getCode()
            );
        } catch (\Exception $e) {
            return $this->_prepareError(
                'General exception: ' . $e->getMessage(),
                $e->getCode()
            );
        }
    }

    /**
     * @param string $message
     * @param int $code
     * @return array
     */
    protected function _prepareError(string $message, int $code): array
    {
        $this
            ->_loggerInterface
            ->warning('Code: ' . $code . ', Msg: ' . $message);

        return $error = [
            'status' => false,
            'error' => [
                'code' => $code,
                'msg' => $message
            ]
        ];
    }

    /**
     * @param OpenWeatherMap\CurrentWeather $weather
     * @return array
     */
    protected function _prepareData(OpenWeatherMap\CurrentWeather $weather): array
    {
        return [
            'status' => true,
            'weather' => [
                'service_type' => self::SERVICE_TYPE,
                'coord' => [
                    'lat' => $weather->city->lat,
                    'lon' => $weather->city->lon
                ],
                'weather_id' => $weather->weather->id,
                'weather_description' => $weather->weather->description,
                'weather_icon_url' => $weather->weather->getIconUrl(),
                'last_update' => $weather
                    ->lastUpdate
                    ->setTimezone(new \DateTimeZone($this->_timezone->getConfigTimezone()))
                    ->format('Y-m-d H:i:s'),
                'country' => $weather->city->country,
                'city' => $weather->city->name,
                'cloud_value' => $weather->clouds->getValue(),
                'cloud_unit' => WeatherUnitTypeInterface::PERCENT_UNIT,
                'humidity_value' => $weather->humidity->getValue(),
                'humidity_unit' => WeatherUnitTypeInterface::PERCENT_UNIT,
                'precipitation_value' => $weather->precipitation->getValue(),
                'precipitation_unit' => WeatherUnitTypeInterface::RAIN_UNIT,
                'precipitation_description' => $weather->precipitation->getDescription(),
                'pressure_value' => $weather->pressure->getValue(),
                'pressure_unit' => WeatherUnitTypeInterface::PRESSURE_UNIT,
                'temperature_min_value' => $weather->temperature->min->getValue(),
                'temperature_min_unit' => $weather->temperature->min->getUnit(),
                'temperature_max_value' => $weather->temperature->max->getValue(),
                'temperature_max_unit' => $weather->temperature->max->getUnit(),
                'temperature_now_value' => $weather->temperature->now->getValue(),
                'temperature_now_unit' => $weather->temperature->now->getUnit(),
                'wind_direction_value' => $weather->wind->direction->getValue(),
                'wind_direction_unit' => $weather->wind->direction->getUnit(),
                'wind_direction_description' => $weather->wind->direction->getDescription(),
                'wind_speed_value' => $weather->wind->speed->getValue(),
                'wind_speed_unit' => $weather->wind->speed->getUnit(),
                'wind_speed_description' => $weather->wind->speed->getDescription()
            ]
        ];
    }
}
