<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Service;

interface ServiceInitializeInterface
{
    /**
     * @return ServiceInitializeInterface
     */
    public function init(): ServiceInitializeInterface;

    /**
     * @return array
     */
    public function call(): array;
}
