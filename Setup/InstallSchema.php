<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Setup;

use DominJed\Weather\Model\Definition\WeatherInformation\TableDefinitionInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists(
            $installer->getTable(TableDefinitionInterface::TABLE_NAME)
        )
        ) {
            $table = $installer
                ->getConnection()
                ->newTable(
                    $installer->getTable(TableDefinitionInterface::TABLE_NAME)
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    10,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'service_type',
                    Table::TYPE_TEXT,
                    24,
                    [
                        'nullable' => false
                    ],
                    'Service type'
                )
                ->addColumn(
                    'coord',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Coord'
                )
                ->addColumn(
                    'weather_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Weather id'
                )
                ->addColumn(
                    'weather_description',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Weather description'
                )
                ->addColumn(
                    'weather_icon_url',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Weather icon url'
                )
                ->addColumn(
                    'last_update',
                    Table::TYPE_DATETIME,
                    null,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Last update weather in weather serwer'
                )
                ->addColumn(
                    'country',
                    Table::TYPE_TEXT,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Country short code'
                )
                ->addColumn(
                    'city',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'City name'
                )
                ->addColumn(
                    'cloud_value',
                    Table::TYPE_INTEGER,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Cloud value'
                )
                ->addColumn(
                    'cloud_unit',
                    Table::TYPE_TEXT,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Cloud unit'
                )
                ->addColumn(
                    'humidity_value',
                    Table::TYPE_INTEGER,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Humidity value'
                )
                ->addColumn(
                    'humidity_unit',
                    Table::TYPE_TEXT,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Humidity unit'
                )
                ->addColumn(
                    'precipitation_value',
                    Table::TYPE_INTEGER,
                    5,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Precipitation value'
                )
                ->addColumn(
                    'precipitation_unit',
                    Table::TYPE_TEXT,
                    12,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Precipitation unit'
                )
                ->addColumn(
                    'precipitation_description',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Precipitation description'
                )
                ->addColumn(
                    'pressure_value',
                    Table::TYPE_INTEGER,
                    4,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Pressure value'
                )
                ->addColumn(
                    'pressure_unit',
                    Table::TYPE_TEXT,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Pressure unit'
                )
                ->addColumn(
                    'temperature_min_value',
                    Table::TYPE_DECIMAL,
                    '5,2',
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Temperature minimal'
                )
                ->addColumn(
                    'temperature_max_value',
                    Table::TYPE_DECIMAL,
                    '5,2',
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Temperature maximal'
                )
                ->addColumn(
                    'temperature_now_value',
                    Table::TYPE_DECIMAL,
                    '5,2',
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Temperature current'
                )
                ->addColumn(
                    'temperature_min_unit',
                    Table::TYPE_TEXT,
                    12,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Temperature min. unit'
                )
                ->addColumn(
                    'temperature_max_unit',
                    Table::TYPE_TEXT,
                    12,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Temperature max. unit'
                )
                ->addColumn(
                    'temperature_now_unit',
                    Table::TYPE_TEXT,
                    12,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Temperature now unit'
                )
                ->addColumn(
                    'wind_direction_value',
                    Table::TYPE_INTEGER,
                    3,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Wind direction value'
                )
                ->addColumn(
                    'wind_direction_unit',
                    Table::TYPE_TEXT,
                    4,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Wind direction unit'
                )
                ->addColumn(
                    'wind_direction_description',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Wind direction description'
                )
                ->addColumn(
                    'wind_speed_value',
                    Table::TYPE_DECIMAL,
                    '4,1',
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Wind speed value'
                )
                ->addColumn(
                    'wind_speed_unit',
                    Table::TYPE_TEXT,
                    12,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Wind speed unit'
                )
                ->addColumn(
                    'wind_speed_description',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => true,
                        'default' => null
                    ],
                    'Wind speed description'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT
                    ],
                    'Created at'
                )
            ;
            $installer
                ->getConnection()
                ->createTable($table);
        }

        $setup->endSetup();
    }
}
