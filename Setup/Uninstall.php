<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
namespace DominJed\Weather\Setup;

use DominJed\Weather\Model\Definition\WeatherInformation\TableDefinitionInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    /**
     * {@inheritDoc}
     */
    public function uninstall(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;
        $installer->startSetup();

        $installer
            ->getConnection()
            ->dropTable($installer->getTable(TableDefinitionInterface::TABLE_NAME));

        $installer->endSetup();
    }
}
