<?php
/**
 * @author DominJed <dev@vprogs.com>
 */
use Magento\Framework\Component\ComponentRegistrar as CR;

CR::register(CR::MODULE, 'DominJed_Weather', __DIR__);
