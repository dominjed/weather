define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/section-config'
], function (Component, customerData, config) {
    'use strict';

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();

            //For first load without any data
            this.dmj_weather_section = function() {
                return { status: false, data: [] };
            };

            customerData.reload('dmj_weather_section');
            this.dmj_weather_section = customerData.get('dmj_weather_section');
        }
    });
});
